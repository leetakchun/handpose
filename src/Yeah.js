// Import Dependencies
// Data file from https://github.com/andypotato/fingerpose
import {Finger, FingerCurl, FingerDirection, GestureDescription} from 'fingerpose';

// Define Gesture Description
// new Class --> 即是object, addCrul()等即是method
export const Yeah = new GestureDescription('Yeah');

// 數值是指confident value, 有幾接近所要的動作
// Thumb 母指 
Yeah.addCurl(Finger.Thumb, FingerCurl.NoCurl, 0.9);//1.0是容許度，1.0是完全一樣才計
Yeah.addDirection(Finger.Thumb, FingerDirection.VerticalUp, 0.8);
Yeah.addDirection(Finger.Thumb, FingerDirection.DiagonalUpLeft, 0.5);
Yeah.addDirection(Finger.Thumb, FingerDirection.DiagonalUpRight, 0.5);
// //0.25是容許度, 即可容許少少buffer

// // Pinky 尾指
Yeah.addCurl(Finger.Pinky, FingerCurl.NoCurl, 0.9);//1.0是容許度，1.0是完全一樣才計
Yeah.addDirection(Finger.Pinky, FingerDirection.VerticalUp, 0.8);
Yeah.addDirection(Finger.Pinky, FingerDirection.DiagonalUpLeft, 0.5);
Yeah.addDirection(Finger.Pinky, FingerDirection.DiagonalUpRight, 0.5);


for(let finger of [ Finger.Index, Finger.Middle, Finger.Ring]){
    Yeah.addCurl(finger, FingerCurl.FullCurl, 0.85);//1.0是容許度，1.0是完全一樣才計
    // loveyouGesture.addDirection(finger, FingerDirection.VerticalDown, 0.25);
}

