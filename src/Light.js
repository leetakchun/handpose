// Import Dependencies
// Data file from https://github.com/andypotato/fingerpose
import {Finger, FingerCurl, FingerDirection, GestureDescription} from 'fingerpose';

// Define Gesture Description
// new Class --> 即是object, addCrul()等即是method
export const Light = new GestureDescription('Light');

// 數值是指confident value, 有幾接近所要的動作
// Thumb 母指 
// loveyouGesture.addCurl(Finger.Thumb, FingerCurl.NoCurl, 0.8);//1.0是容許度，1.0是完全一樣才計
// loveyouGesture.addDirection(Finger.Thumb, FingerDirection.Horizontalright, 0.8);
// loveyouGesture.addDirection(Finger.Thumb, FingerDirection.DiagonalDownLeft, 0.5);
// loveyouGesture.addDirection(Finger.Thumb, FingerDirection.DiagonalDownRight, 0.5);
// //0.25是容許度, 即可容許少少buffer

// // Index 食指
// loveyouGesture.addCurl(Finger.Index, FingerCurl.NoCurl, 0.8);//1.0是容許度，1.0是完全一樣才計
// loveyouGesture.addDirection(Finger.Index, FingerDirection.VerticalUp, 0.25);

// // Pinky 尾指
// loveyouGesture.addCurl(Finger.Pinky, FingerCurl.NoCurl, 0.8);//1.0是容許度，1.0是完全一樣才計
// loveyouGesture.addDirection(Finger.Pinky, FingerDirection.VerticalUp, 0.25);


for(let finger of [Finger.Thumb, Finger.Index, Finger.Middle, Finger.Ring, Finger.Pinky]){
    Light.addCurl(finger, FingerCurl.NoCurl, 0.75);//1.0是容許度，1.0是完全一樣才計
    // loveyouGesture.addDirection(finger, FingerDirection.VerticalDown, 0.25);
}

//js for..of loop test
// for (const element of ["Finger1", "Finger2"]) {
//     console.log(`This is a test for ${element}`);
//   }
