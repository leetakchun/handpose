// Drawing function
export const games = (predictions, ctx, canvas, enemies, animationId, projectiles, particles) => {
  class Player{
    constructor(x,y,radius,color){
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
    }
    draw(){
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, false);
        ctx.fillStyle = this.color;
        ctx.fill()
    }
  }

  class Projectile{
    constructor(x,y,radius,color, velocity){
        this.x = x; //係canvas 的x 座標
        this.y = y; //係canvas 的y 座標
        this.radius = radius;
        this.color = color;
        this.velocity = velocity;
    }
    draw(){
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, false);
        ctx.fillStyle = this.color;
        ctx.fill()
    }
    update(){
        this.draw();
        this.x = this.x + this.velocity.x;
        this.y = this.y + this.velocity.y;
    }
}

const friction = 0.99;
class Particle{
    constructor(x,y,radius,color, velocity){
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.velocity = velocity;
        this.alpha = 1;
    }
    draw(){
        //因為要用globalAlpha, 所以頭尾要有save()及restore()
        ctx.save();
        ctx.globalAlpha = this.alpha;
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI*2, false);
        ctx.fillStyle = this.color;
        ctx.fill()
        ctx.restore()
    }
    update(){
        this.draw();
        this.velocity.x *= friction //減速
        this.velocity.y *= friction //減速
        this.x = this.x + this.velocity.x;
        this.y = this.y + this.velocity.y;
        this.alpha -= 0.01;
    }
}

  const xx = canvas.width / 2;
  const yy = canvas.height /2 ;   

//   function init(){
//     player = new Player(x,y,10,'white');
//     projectiles = []
//     enemies = []
//     particles = []
//     window.clearInterval(timeoutID)//無clearTime interval便會restart game一次過出好多enemy!
// }

  let player = new Player(xx , yy,10,'white');
  player.draw();



  projectiles.forEach((projectile, index) =>{ //再run發射點
    projectile.update();
    //出左畫面就remove發射點, 減低運算 !! 好重要 !!!
    if( projectile.x + projectile.radius <0 || 
        projectile.x - projectile.radius > canvas.width||
        projectile.y + projectile.radius < 0 ||
        projectile.y - projectile.radius > canvas.height
        ){
        setTimeout(()=>{
            projectiles.splice(index,1); //移除在版面外的點
        } , 0)
    }
    //發射點的dist, 證明是可以中了或出了screen便不會再計算Math.hypot
});

  //會爆好多個出嚟，因為spawnEnemies會由16.7ms計出來
  // spawnEnemies(); //由外部計算好引入enemies變數
  enemies.forEach((enemy, index)=>{
    enemy.update();

    const dist = Math.hypot(player.x -enemy.x, player.y - enemy.y);
    if(dist - enemy.radius - player.radius < 1){
        //modalEl.style.display = "flex"; //重要display是flex
        //bigScoreEl.innerHTML = scoreValue;
        console.log(`The animationId is ${animationId}`); // stop ok
        cancelAnimationFrame(animationId); //html內置的function, 可將相應id 暫停
      }//end of checking the enemies touch the center



              //Math.hypo(x - dx,y - dy)是計算兩點的距離 (collision detection)
              projectiles.forEach((projectile, projectileIndex) => {
                const dist = Math.hypot(projectile.x -enemy.x, projectile.y - enemy.y);
                
                //when projectile touch enemy
                if(dist - enemy.radius - projectile.radius <1){
    
                    
                    if(enemy.radius > 10){
                    //increase our score
                    // scoreValue += 100
                    // scoreEl.innerHTML = scoreValue;
    
                        enemy.radius -=10; //是現在當下的enemy 的 radius, 不用enemies.splice(index,1, new enemy())這樣煩覆
                    //!!!以下的setTimeout是用來避免array更新時有閃跳flash現象!!!
                        //用gsap是可以使縮少是逐漸去減少
                        // gsap.to(enemy, {
                        //     radius: enemy.radius - 10 
                        // })

                        projectiles.splice(projectileIndex,1)
                        // setTimeout(()=>{
                        //     projectiles.splice(projectileIndex,1);
                        // }, 0);//超必，避免閃跳flash現象!  
                        
                        
                    }else{
    
                    //increase our score when enemy remove from screen
                    // scoreValue += 250
                    // scoreEl.innerHTML = scoreValue;
                      

                    enemies.splice(
                      index,1, )
                           //splice(start, deleteCount, additem1, additem2, ...)
                  projectiles.splice(projectileIndex,1);
                        // setTimeout(()=>{
                        //     enemies.splice(
                        //         index,1, )
                        //              //splice(start, deleteCount, additem1, additem2, ...)
                        //     projectiles.splice(projectileIndex,1);
                        // }, 0);  
                        
                    }
    
                }
                // console.log(dist)//是發射點的dist, 證明是可以中了或出了screen便不會再計算Math.hypot
            });
            
  })

  canvas.addEventListener('click', (event)=>{
    console.log(projectiles);
    //angle計算 radian value
    const angle = Math.atan2(
        event.clientY - canvas.height/2, 
        event.clientX - canvas.width/2)
        //console.log(angle);
    const velocity = { //由長度來表示速度，因為是 x + velocity.x, 即是變化的長度，便是速度
        x: Math.cos(angle) * 6,
        y: Math.sin(angle) * 6
    }
    //用array來加新的粒子
    projectiles.push(new Projectile(
        canvas.width/2, canvas.height/2,
        5, 'white', velocity 
    ))
});

  // Check if we have predictions
  if (predictions.length > 0) {
    // Loop through each prediction
    predictions.forEach((prediction) => {
      // Grab landmarks
      const landmarks = prediction.landmarks;
      console.log(landmarks)



    });
  }
};