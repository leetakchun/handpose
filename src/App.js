import React, {useRef, useEffect, useState} from 'react';
import * as tf from "@tensorflow/tfjs";
import * as handpose from "@tensorflow-models/handpose";
import Webcam from "react-webcam"
//import {TweenMax, Power3, gsap, TimelineLite, TimelineMax} from 'gsap'; 
// import logo from './logo.svg';
import './App.css';
import {drawHand} from "./utilities";
// import { games } from './games';
// import {canvasGame} from './canvasGame';
// import {canvasGameSimple} from './canvasGameSimple';

import {Light} from './Light';
import {Yeah} from './Yeah';

//Import new stuff
import * as fp from "fingerpose";
import victory from "./victory.png";
import thumbs_up from "./thumbs_up.png";



function App() {
  const webcamRef = useRef(null);
  const canvasRef = useRef(null);

  let [circle, setCircle] = useState(() => []);

  let [handPosition, setHandPosition] = useState(() => []);

    //useState(0) re-render will run function
  let [circleX, setCircleX] = useState(() => 0);
  let [circleY, setCircleY] = useState(() => 0);

  //set state for hand
  const [emoji, setEmoji] = useState(() => null);
  const images = {thumbs_up:thumbs_up, victory:victory};
  const [text, setText] = useState(() => null);
  const [handTouch, setHandTouch] = useState(() => null)

  const requestRef = useRef();
  const requestRefCircle = useRef();

  //set vaiable initial state
  let x =210 ;
  let y = 200;  
  let dx = 20;
  let dy =20;
  let radius =30;
  let color = "aqua";
  const velocity = {x:10, y: 10};


  class circleMoving{
    constructor(x,y,radius,color,velocity){
      this.x = x;
      this.y = y;
      this.radius = radius;
      this.color = color;
      this.velocity = velocity;
    }
    draw(){
      const ctx = canvasRef.current.getContext("2d");
      ctx.save();
      ctx.beginPath();
      ctx.arc(this.x,this.y,this.radius,0,Math.PI *2, false);
      ctx.fillStyle = this.color;
      ctx.fill();
      ctx.restore();
    }
    update(){
      this.draw();
      if(this.x+ this.radius + this.velocity.x > canvasRef.current.width || this.x - this.radius < 0 ){ //bug x-radius
        this.velocity.x = -this.velocity.x ;
        }
      if(this.y+ this.radius + this.velocity.y > canvasRef.current.height || this.y - this.radius < 0 ){ //bug y-radius
        this.velocity.y = -this.velocity.y ;
        }
      this.x = this.x + this.velocity.x;
      this.y = this.y + this.velocity.y;

      setCircleX(preX => preX = this.x); //update the value
      setCircleY(preY => preY = this.y); //update the value
    }
  }
  
  let [ball, setBall] = useState(()=> new circleMoving(x,y, radius, "red", velocity));
  

  const detect = async (net) => {
    // Check data is available
    if (
      typeof webcamRef.current !== "undefined" &&
      webcamRef.current !== null &&
      webcamRef.current.video.readyState === 4
    ) {
      // Get Video Properties
      const video = webcamRef.current.video;
      const videoWidth = webcamRef.current.video.videoWidth;
      const videoHeight = webcamRef.current.video.videoHeight;

      // Set video width
      webcamRef.current.video.width = videoWidth;
      webcamRef.current.video.height = videoHeight;

      // Set canvas height and width
      canvasRef.current.width = videoWidth;
      canvasRef.current.height = videoHeight;

      // Make Detections
      // net.estimateHands(video,true) 加了true便是真實鏡射來處理
      const hand = await net.estimateHands(video, true);
      const ctx = canvasRef.current.getContext("2d");
      

      // A --> detect gesture and draw
      if(hand.length >0){ //畫面有手出現便run以下code
        const GE = new fp.GestureEstimator([
          fp.Gestures.VictoryGesture,
          fp.Gestures.ThumbsUpGesture,
          Light,
          Yeah
        ]);
        //可以加新custom gesture
         const gesture = await GE.estimate(hand[0].landmarks, 7.5);
        //console.log(gesture.gestures);
        //當偵測到真係有相應的動作出現，便run以下程式
        if(gesture.gestures !== undefined && gesture.gestures.length>0){
          const confidence =  gesture.gestures.map(
            (prediction) => prediction.confidence
          );//這是array
          const maxConfidence = confidence.indexOf(
            Math.max.apply(null, confidence)
          );//這是array內最大機會出現的動作的index數字
          setEmoji(gesture.gestures[maxConfidence].name); //設定state,當條件合適
          //console.log(`The maxConfidence is ${confidence[0]}`);
          // console.log(maxConfidence);

              if(gesture.gestures[0].name === "Light"){
                setText("Light !!!!!")
              }
              if(gesture.gestures[0].name === "Yeah"){
                setText("Yahoooooooo~~~")
              }
        }
      
          // B --> detect hand and Draw mesh
          // drawHand
          drawHand(hand, ctx);

          //C --> store hand position into state
          setHandPosition(hand[0].landmarks)
          
      }else{//run code if no hand detected
        setEmoji(null);
        setText(null);
        setHandTouch(null);
      }
      // Animation can store here
    }//end of webcam checking
  };//end of detect

  const runCircle = () =>{
    function circleAnimation(){
      requestRefCircle.current = requestAnimationFrame(circleAnimation);
      ball.update(); //question 1 (the red ball cannot be drawed at the beginning, only drawe the orange ball when the finger is touched the red ball...)
    }
    requestRefCircle.current = requestAnimationFrame(circleAnimation);
  }

  const runHandpose = async () => {
    //outside of animation
    const net = await handpose.load();
    console.log("Handpose model loaded.");
    async function frameLandmarks(){
      //animation start
      requestRef.current = requestAnimationFrame(frameLandmarks)
      detect(net);
      ball.update(); //question 2 (if no this ball.update, the red ball will not be drawed in the beginning)
    }
   requestRef.current = requestAnimationFrame(frameLandmarks)
  };

//collision detection
  useEffect(()=>{ 
    console.log(ball)
    if(handPosition.length>0){
      let dist = Math.hypot(handPosition[8][0] - circleX, handPosition[8][1] - circleY);
      if(dist - radius - 12 < 10){
        setHandTouch("TOUCHED")
        let newState = new circleMoving(circleX, circleY,radius,"orange", velocity)
        setBall(preState => 
           preState = newState
        );
        console.log("It is touched by hand");
    }
      }
  },[handPosition]) 

  //render the ball
  useEffect(()=>{
    runCircle();
    return()=>{
      cancelAnimationFrame(requestRefCircle.current);
    }
  }, [ball.color])


  useEffect(()=>{
    runHandpose();
    //similar to componentDidMount
    return()=> {
      cancelAnimationFrame(requestRef.current);
      //similar to componentWillUnmount
    }
  },[]);//component will Update

  return (
    <div className="App">
      <header className="App-header">
        <Webcam
          ref={webcamRef}
          mirrored={true}
          videoConstraints={{
            facingMode:"user"
          }}
          style={{
            position: "absolute",
            marginLeft: "auto",
            marginRight: "auto",
            left: 0,
            right: 0,
            textAlign: "center",
            zindex: 9,
            width: 640,
            height: 480,
          }}
        />

        <canvas
          ref={canvasRef}
          style={{
            position: "absolute",
            marginLeft: "auto",
            marginRight: "auto",
            left: 0,
            right: 0,
            textAlign: "center",
            zindex: 9,
            width: 640,
            height: 480,
          }}
        />

        {emoji !== null ? <img src={images[emoji]} style={{
          position:"absolute",
          marginLeft:"auto",
          marginRight:"auto",
          top:200,
          left:400,
          bottom:500,
          right:0,
          textAlign:"center",
          height:100
        }}/>:''}

            <h3 style={{
                  position:"absolute",
                  marginLeft:"auto",
                  marginRight:"auto",
                  top:400,
                  left:400,
                  bottom:500,
                  right:0,
                  textAlign:"center",
                  height:100,
                  color:"#66ff99"
              }}>{text}
              </h3> 

              <h3 style={{
                  position:"absolute",
                  marginLeft:"auto",
                  marginRight:"auto",
                  top:400,
                  left:50,
                  bottom:500,
                  right:0,
                  textAlign:"center",
                  height:100,
                  color:"green"
              }}>{handTouch}
              </h3>    

      </header>
    </div>
  );
}

export default App;