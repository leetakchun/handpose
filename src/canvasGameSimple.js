//import {sth} from 'sth';

export let canvasGameSimple =  (c,canvas, x, y, dx, dy, radius, color) =>{

  function Circle(){
    //public properties, 指向新object的屬性
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;
    
    this.draw = function(){
    c.beginPath();
    c.arc(this.x,this.y,this.radius,0,Math.PI *2, false);
    c.fillStyle = color;
    c.fill();
    }
    
    this.update = function(){
     if(this.x+ this.radius > canvas.current.width || this.x < 0 ){ //bug x-radius
        this.dx = -this.dx;
    }
    
    if(this.y+ this.radius > canvas.current.height || this.y - this.radius < 0){
      this.dy = -this.dy;
    }
    //由requestAnimationFrame, 來不繼地更新this.x, this.y value
      this.x += this.dx; 
      this.y += this.dy;
      this.draw();//run draw function when run update function
      console.log("circle update ok") 

      //update function只run
    } 
    this.hello = ()=>{
      console.log("hello")
    }
  }

  let circle = new Circle( x, y, dx, dy, radius, color);
//  canvasGameSimple(ctx, canvasRef, x, y, dx, dy, radius, color)
  // // circle.update()

  return circle
}




