import React, {useRef, useEffect, useState, useCallback} from 'react';
import * as tf from "@tensorflow/tfjs";
import * as handpose from "@tensorflow-models/handpose";
import Webcam from "react-webcam"
import {TweenMax, Power3, gsap, TimelineLite, TimelineMax} from 'gsap'; 
// import logo from './logo.svg';
import './App.css';
import {drawHand} from "./utilities";
import { games } from './games';
import {canvasGame} from './canvasGame';
import {canvasGameSimple} from './canvasGameSimple';

import {Light} from './Light';
import {Yeah} from './Yeah';

//Import new stuff
import * as fp from "fingerpose";
import victory from "./victory.png";
import thumbs_up from "./thumbs_up.png";



function App() {
  const webcamRef = useRef(null);
  const canvasRef = useRef(null);

  let [enemies, setEnemies] = useState(() => []);
  let [handPosition, setHandPosition] = useState(() => []);
  let [circle, setCircle] = useState(() => {
    console.log('run function initial only 1 time') 
    return 0
  });
  let [circleX, setCircleX] = useState(() => 0);
  let [circleY, setCircleY] = useState(() => 0);
  //若用useState(0)便會每次re-render也會run function

  //set state for hand
  const [emoji, setEmoji] = useState(() => null);
  const images = {thumbs_up:thumbs_up, victory:victory};
  const [text, setText] = useState(() => null);

  //images[thumbs_up] 即是等於是 ./static/img/thumbs_up.png 之類的link
  //console.log("Main React Render"); //images png link

  // let animationId;
  const requestRef = useRef();
  const requestRefSimple = useRef();

  let x =210 ;
  let y = 200;  
  let dx = 20;
  let dy =20;
  let radius =30;
  let color = "aqua";



  let gamesSimple = () => {
        const ctx = canvasRef.current.getContext("2d");
        ctx.save();
        ctx.beginPath();
        ctx.arc(x,y,radius,0,Math.PI *2, false);
        ctx.fillStyle = color;
        ctx.fill();
        ctx.restore();
        if(x+ radius +dx > canvasRef.current.width || x - radius < 0 ){ //bug x-radius
          dx = -dx;
          }
        if(y+ radius +dy > canvasRef.current.height || y - radius  < 0){
        dy = -dy;
        }
      //由requestAnimationFrame, 來不繼地更新x, y value
        x += dx; 
        y += dy;   
        // console.log(`this x is ${x}`)
        // console.log(`this y is ${y}`)
        // setCircle(state => state=x) ok
    }
  


  const detect = async (net) => {
    // Check data is available
    if (
      typeof webcamRef.current !== "undefined" &&
      webcamRef.current !== null &&
      webcamRef.current.video.readyState === 4
    ) {
      // Get Video Properties
      const video = webcamRef.current.video;
      const videoWidth = webcamRef.current.video.videoWidth;
      const videoHeight = webcamRef.current.video.videoHeight;

      // Set video width
      webcamRef.current.video.width = videoWidth;
      webcamRef.current.video.height = videoHeight;

      // Set canvas height and width
      canvasRef.current.width = videoWidth;
      canvasRef.current.height = videoHeight;

      // Make Detections
      //net.estimateHands(video,true) 加了true便是真實鏡射來處理
      const hand = await net.estimateHands(video, true);
      const ctx = canvasRef.current.getContext("2d");
      
      //circle.update() ??? undefined --> option1 要用promise define 及await
      //因為同步run conding, circle未取得相應資料 
      gamesSimple();
      setCircle(preState => preState = x);
      setCircleX(preX => preX = x);
      setCircleY(preY => preY = y);

      // A --> detect gesture and draw
      if(hand.length >0){ //畫面有手出現便run以下code
        const GE = new fp.GestureEstimator([
          fp.Gestures.VictoryGesture,
          fp.Gestures.ThumbsUpGesture,
          Light,
          Yeah
        ]);
        //可以加新custom gesture
        //導入hand prediction再分析應用
        // using a minimum confidence of 8 (out of 10)
        //console.log(hand[0].landmarks) //ok
        //之前一直串錯字 hand[0].landmakrs  "k" "r"錯位 !!!!
         const gesture = await GE.estimate(hand[0].landmarks, 7.5);
        //console.log(gesture.gestures);
        //當偵測到真係有相應的動作出現，便run以下程式
        if(gesture.gestures !== undefined && gesture.gestures.length>0){
          const confidence =  gesture.gestures.map(
            (prediction) => prediction.confidence
          );//這是array
          const maxConfidence = confidence.indexOf(
            Math.max.apply(null, confidence)
          );//這是array內最大機會出現的動作的index數字
          setEmoji(gesture.gestures[maxConfidence].name); //設定state,當條件合適
          //console.log(`The maxConfidence is ${confidence[0]}`);
          // console.log(maxConfidence);

              if(gesture.gestures[0].name == "Light"){
                setText("Light !!!!!")
              }
              if(gesture.gestures[0].name == "Yeah"){
                setText("Yahoooooooo~~~")
              }
        }
          // 在這個地方會undefined，因為未必會即時有合適的動作可使gesture生成 value要>7.5
          // 所以要在以上的地方推斷不是undefined及有length才可run
          // if(gesture.gestures[0].name == "I_Love_You"){
          //   console.log("LOVE !!!!!!")
      
          // B --> detect hand and Draw mesh
          // 有hand才畫手

          drawHand(hand, ctx);


          //C --> store hand position into state
          setHandPosition(hand[0].landmarks)
          

      }else{//當畫便無手出現便run以下code !!!!!

        setEmoji(null);//設定state,當畫面無手出現
        setText(null);

      }

      // 這個位置的code是會跟requestAnimationFrame()不斷run!!!
      // console.log("detect() function running")
      // 動畫應放在這裹!!!


    }//end of webcam checking

  };//end of detect

  const runHandpose = async () => {
    //無loop, 可放class的設定
    //video setting 不可在detect, 因為畫面會不斷flashing
    const net = await handpose.load();
    console.log("Handpose model loaded.");

    //用了useEffect()便只會load一次，之後animation便自行run
    
    async function frameLandmarks(){
      //loop開始，主要放draw動畫或detect設定
      //animationId 要先define之後才run detect()這才可以有current的ID，才可叫停
      requestRef.current = requestAnimationFrame(frameLandmarks)
      detect(net);
      // setCircle((prev)=> prev + x )
      //用function去表示才可運用幾個setCircle而不會override
      
      // setCircle(preState => preState = x+1)
    }
  //   //要用requestRef.current來save底animation ID
   requestRef.current = requestAnimationFrame(frameLandmarks)
  };

  //用useEffect 及return來使effect只會run一之，可用useState之餘及不會累積下去!!!極重要
  //使動畫𣈱順，非常極重要skill !!!
  //若無condition會變成無限loop, 會reject React Hook

  //因為useState是async，由web api執行，不是由Java Engine 執行
  //所以要由useEffect來去console.log(useState value)及執行與react Hook有關的事項
  useEffect(()=>{
    if(circle>300){
      //用function去表示才可運用幾個setCircle而不會override
      setCircle(state => state = "circle > 300")
    }else if(circle < 100){
      setCircle(state => state = "circle == ok")
    }
    // console.log(handPosition)
  },[circle, handPosition]) //因為是每秒都有改變，所以不設定"[]" useEffect(xxx,[]), 或設定circle,handPosition


  useEffect(()=>{ 
    if(handPosition.length>0){
      let dist = Math.hypot(handPosition[8][0] - circleX, handPosition[8][1] - circleY);
      if(dist - radius - 12 < 10){
        
        console.log("SUPER!!!!!!!!!!!!!!!!!");
    }
      }
  },[handPosition]) //檢測有手出現才run

  useEffect(()=>{
    runHandpose();
    //similar to componentDidMount
    //建立 and 更新元件時會執行的function
    return()=> {
      cancelAnimationFrame(requestRef.current);
      //similar to componentWillUnmount
      //移除元件的時會執行的function
    }
  },[]);//用來限制副作用要以哪些state和props作為觸發條件的array

//   useEffect(() => {      
//     gsap.from('#square', {duration:10, scale: 10})
// }, []); 也是慢格


  return (
    <div className="App">
      <header className="App-header">
        <Webcam
          ref={webcamRef}
          mirrored={true}
          videoConstraints={{
            facingMode:"user"
          }}
          style={{
            position: "absolute",
            marginLeft: "auto",
            marginRight: "auto",
            left: 0,
            right: 0,
            textAlign: "center",
            zindex: 9,
            width: 640,
            height: 480,
          }}
        />

        <canvas
          ref={canvasRef}
          style={{
            position: "absolute",
            marginLeft: "auto",
            marginRight: "auto",
            left: 0,
            right: 0,
            textAlign: "center",
            zindex: 9,
            width: 640,
            height: 480,
          }}
        />

        {emoji !== null ? <img src={images[emoji]} style={{
          position:"absolute",
          marginLeft:"auto",
          marginRight:"auto",
          top:200,
          left:400,
          bottom:500,
          right:0,
          textAlign:"center",
          height:100
        }}/>:''}

            <h3 style={{
                  position:"absolute",
                  marginLeft:"auto",
                  marginRight:"auto",
                  top:400,
                  left:400,
                  bottom:500,
                  right:0,
                  textAlign:"center",
                  height:100,
                  color:"#66ff99"
              }}>{text}
                {circle}
              </h3>   

          {/* <div id="square" 
               style={{
                backgroundColor: "orange",
                height: "5em",
                width: "5em",
                position:"absolute",
                marginLeft:"auto",
                marginRight:"auto",
                top:400,
                left:100,
                right:0,
                textAlign:"center",
                height:100,
                borderRadius: "100%"
          }}></div> */}

      </header>
    </div>
  );
}

export default App;