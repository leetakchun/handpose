//import {sth} from 'sth';

export const canvasGame =  (c,canvas, enemies, setEnemies) =>{


  function Circle(x, y, dx, dy, radius, color){
    //public properties, 指向新object的屬性
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;
    
    this.draw = function(){
    c.beginPath();
    c.arc(this.x,this.y,this.radius,0,Math.PI *2, false);
    c.strokeStyle = color;
    c.stroke();
    }
    
    this.update = function(){
     if(this.x+ this.radius > canvas.current.width || this.x < 0 ){ //bug x-radius
        this.dx = -this.dx;
    }
    
    if(this.y+ this.radius > canvas.current.height || this.y - this.radius < 0){
      this.dy = -this.dy;
    }
    //由requestAnimationFrame, 來不繼地更新this.x, this.y value
      this.x += this.dx;
      this.y += this.dy;
      this.draw();//run draw function when run update function
    } 
  }

  let circle = new Circle(200, 200 ,5,5,30);
  // circle.update()


  class Enemy{
    constructor(x,y,radius,color, velocity){
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.velocity = velocity;
    }
    draw(){
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI*2, false);
        c.fillStyle = this.color;
        c.fill()
    }
    update(){
        this.draw();
        this.x = this.x + this.velocity.x;
        this.y = this.y + this.velocity.y;
    }
  }

    //每一秒走出來
        const radius = (Math.random() * (30 - 4)) + 4; //size 由 4 至 30出現
        let x;
        let y;
        //可全面在四方八面來
        //分50：50機會出現，再分別控制x,y的軸是全random, 另一個就是在canvas出現
        if(Math.random() < 0.5){
            //x一定是左或右出現，無中間數。y是全軸random數
            x = Math.random() < 0.5 ? 0 - radius : canvas.current.width + radius;
            y = Math.random() * canvas.current.height;
        }else{
            //y一定是上或下出現，無中間數。x是全軸random數
            x = Math.random() * canvas.current.width;
            y = Math.random() < 0.5 ? 0 - radius : canvas.current.height + radius;
        }
        const color= `hsl(${Math.random()*360}, 50%, 50%)`;//random color

        const angle = Math.atan2(
            canvas.current.height/2 - y, 
            canvas.current.width/2 - x)
            //console.log(angle);
        const velocity = { //由長度來表示速度，因為是 x + velocity.x, 即是變化的長度，便是速度
            x: Math.cos(angle) ,
            y: Math.sin(angle) 
        }
        //setEnemies(enemies => [...enemies, new Enemy(x, y, radius, color, velocity)])
        // enemies.push(new Enemy(x, y, radius, color, velocity))


  //await setEnemies([3,4,8,8,9,0,9,9]) 時間不同，先return出去
  // const a = new Enemy(x,y,radius,color, velocity);
  // return a

}

